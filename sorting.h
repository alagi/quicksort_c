//
// Created by vargat on 2019. 11. 14..
//

#ifndef RENDEZ_SORTING_H
#define RENDEZ_SORTING_H

int* quickSort(int*, int, int);
int divide(int*, int, int);
void intSwap(int*, int*);

#endif //RENDEZ_SORTING_H
