//
// Created by vargat on 2019. 11. 14..
//

#ifndef RENDEZ_UTIL_H
#define RENDEZ_UTIL_H

#define LOWER_BOUNDARY -10
#define UPPER_BOUNDARY 0
#define SIZE 20

int* randomNumbers();
void printArray(char*, const int*, int);
#endif //RENDEZ_UTIL_H
