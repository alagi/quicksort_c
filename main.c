#include <stdio.h>
#include <stdlib.h>

#include "sorting.h"
#include "util.h"

int main() {

    int* intArray = randomNumbers();

   printArray("orig:", intArray, SIZE);

   printArray("rend:", quickSort(intArray,0, SIZE-1),SIZE);

    return EXIT_SUCCESS;
}