//
// Created by vargat on 2019. 11. 14..
//
#include <time.h>
#include <stdio.h>
#include "util.h"
#include <stdlib.h>

int* randomNumbers() {
    void *p = malloc(SIZE * sizeof(int));
    int *intArray = (int *) p;
    int i;
    srand(time(0));
    for (i = 0; i < SIZE; i++) {

        *(intArray + i) = rand() % (UPPER_BOUNDARY - LOWER_BOUNDARY+1) + LOWER_BOUNDARY;
    }
    return intArray;
}

void printArray(char* szoveg, const int* tomb, int size) {
    int i=0;
    printf("%s [ ", szoveg);
    for (i;i<size;i++) {
        printf("%d ", *(tomb+i));
    }
    printf("]\n");
}
