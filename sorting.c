//
// Created by vargat on 2019. 11. 14..
//
#include <stdio.h>
#include "sorting.h"

int *quickSort(int *tomb, int from, int to) {
    int q;
    if (from < to) {
        q = divide(tomb, from, to);
        quickSort(tomb, from, q - 1);
        quickSort(tomb, q + 1, to);
    }
    return tomb;
}

int divide(int *tomb, int from, int to) {
    int key = *(tomb + to);

    int i = from - 1;
    int j;
    for (j = from; j <= to - 1; j++) {
        if (*(tomb + j) < key) {
            i++;

            intSwap((tomb + i), (tomb + j));
        }
    }

    intSwap((tomb + i + 1), (tomb + to));

    return i + 1;
}

void intSwap(int *a, int *b) {
    int temp;
    temp = *a;
    *a = *b;
    *b = temp;
}
